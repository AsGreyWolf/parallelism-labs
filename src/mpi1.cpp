#include <boost/mpi.hpp>
#include <boost/program_options.hpp>
#include <chrono>
#include <random>
#include <thread>

namespace opt = boost::program_options;
namespace mpi = boost::mpi;

bool parse_opts(int argc, char *argv[], size_t *size) {
	opt::options_description opts{};
	opts.add_options()("size,n", opt::value(size)->required())(
	    "help,h", "produce help message");
	opt::variables_map vm;
	opt::store(opt::parse_command_line(argc, argv, opts), vm);
	if (vm.count("help") != 0) {
		std::cout << opts << std::endl;
		return false;
	}
	try {
		opt::notify(vm);
	} catch (const opt::required_option &e) {
		std::cerr << "Error: " << e.what() << std::endl;
		return false;
	}
	return true;
}

size_t slave(mpi::communicator &world, size_t size = 0) {
	mpi::broadcast(world, size, 0);
	size = size / world.size() + (world.rank() < (size % world.size()) ? 1 : 0);

	std::random_device rand;
	std::mt19937 gen(rand());
	size_t found;
	for (size_t i = 0; i < size; i++) {
		auto h = std::uniform_real_distribution<double>{0, 1}(gen);
		auto phi = std::uniform_real_distribution<double>{0, M_PI}(gen);
		if (h <= sin(phi))
			found++;
	}
	size_t result = 0;
	mpi::reduce(world, found, result, std::plus<>{}, 0);
	return result;
}

void master(mpi::communicator &world, size_t size) {
	using clock = std::chrono::steady_clock;
	auto start = clock::now();
	size_t found = slave(world, size);
	auto duration = clock::now() - start;
	std::cout
	    << 2.0l * size / found << ' '
	    << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count()
	    << "ms" << std::endl;
}

int main(int argc, char *argv[]) {
	mpi::environment env{argc, argv};
	mpi::communicator world;
	std::cout << "Started worker " << world.rank() << " of " << world.size()
	          << std::endl;
	if (world.rank() == 0) {
		size_t size;
		if (!parse_opts(argc, argv, &size)) {
			return 1;
		}
		master(world, size);
	} else {
		slave(world);
	}
	//	100000000, 200000000, 300000000, 400000000, 500000000, 600000000, 700000000,
	//	800000000, 900000000, 1000000000
	//	1958, 3755, 5657, 7616, 9511, 11135, 13172,
	//	15263, 16994, 18503
}
