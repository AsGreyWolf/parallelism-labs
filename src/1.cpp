#include <algorithm>
#include <array>
#include <chrono>
#include <iostream>
#include <iterator>
#include <memory>
#include <random>
#include <utility>

namespace {
template <typename T, typename Clock> struct tester {
	template <size_t N, typename Gen> static auto random_matrix(Gen &generator) {
		//	std::array<std::array<T, N>, N> matrix{};
		std::vector<std::array<T, N>> matrix(N);
		for (auto &row : matrix)
			std::generate(row.begin(), row.end(), [&generator] {
				return std::uniform_real_distribution<T>{}(generator);
			});
		return matrix;
	}
	template <size_t N> static auto test(std::ostream &out) {
		std::random_device rand;
		std::mt19937 generator{rand()};
		auto matrix1 = random_matrix<N>(generator);
		auto matrix2 = random_matrix<N>(generator);
		auto result = matrix1;
		auto start = Clock::now();
#pragma omp parallel for schedule(guided, 10)
		for (size_t i = 0; i < N; i++)
			for (size_t j = 0; j < N; j++)
				matrix2[i][j] = matrix2[j][i];
#pragma omp parallel for schedule(guided, 10)
		for (size_t i = 0; i < N; i++)
			for (size_t j = 0; j < N; j++) {
				result[i][j] = 0;
				for (size_t k = 0; k < N; k++) {
					result[i][j] += matrix1[i][k] * matrix2[j][k];
				}
			}
		auto duration = Clock::now() - start;
		for (auto &row : result) {
			std::copy(row.begin(), row.end(), std::ostream_iterator<T>{out, " "});
			out << "\n";
		}
		return duration;
	}
	template <size_t... Ns>
	static auto multitest(std::ostream &out, std::index_sequence<Ns...>) {
		using result_type = std::array<typename Clock::duration, sizeof...(Ns)>;
		result_type result{{test<Ns>(out)...}};
		for (int i = 0; i < 3; i++) {
			result_type temp{test<Ns>(out)...};
			std::transform(result.begin(), result.end(), temp.begin(), result.begin(),
			               [](auto &a, auto &b) { return std::min(a, b); });
		}
		return result;
	}
};
template <size_t Multiplier, size_t... Ns>
auto multiply_sequence(std::index_sequence<Ns...>) {
	return std::index_sequence<(Ns * Multiplier)...>{};
}
} // namespace

int main() {
	std::ios_base::sync_with_stdio(false);
	//	static 1 3482
	//	static 10 3509
	//	static 100 3474
	//	static 500 3543
	//	static 1000 6318
	//	dynamic 1 3435
	//	dynamic 10 3520
	//	dynamic 100 3499
	//	dynamic 500 3468
	//	dynamic 1000 6255
	//	guided 1 3474
	//	guided 10 3457
	//	guided 100 3576
	//	guided 1000 6300
	//	0 200 400 600 800 1000 1200 1400 1600 1800 2000
	//	0 7 62 219 548 1090 1918 3072 4574 6259 9050
	//	0 3 29 89 224 425 724 1175 1754 2563 3484
	auto durations = tester<double, std::chrono::steady_clock>::multitest(
	    std::cout, multiply_sequence<200>(std::make_index_sequence<11>()));
	for (auto &dur : durations) {
		std::cout
		    << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count()
		    << ' ';
	}
}
