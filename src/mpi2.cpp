#include <boost/mpi.hpp>
#include <boost/program_options.hpp>
#include <chrono>
#include <random>
#include <thread>

namespace opt = boost::program_options;
namespace mpi = boost::mpi;

bool parse_opts(int argc, char *argv[], size_t *size) {
	opt::options_description opts{};
	opts.add_options()("size,n", opt::value(size)->required())(
	    "help,h", "produce help message");
	opt::variables_map vm;
	opt::store(opt::parse_command_line(argc, argv, opts), vm);
	if (vm.count("help") != 0) {
		std::cout << opts << std::endl;
		return false;
	}
	try {
		opt::notify(vm);
	} catch (const opt::required_option &e) {
		std::cerr << "Error: " << e.what() << std::endl;
		return false;
	}
	return true;
}

// template <typename T> using matrix = std::vector<std::vector<T>>;

template <typename T> class matrix {
	size_t size_ = 0;
	std::vector<T> items_{};

public:
	matrix() = default;
	matrix(size_t N) : size_{N}, items_(N * N) {}
	size_t size() const { return size_; }
	T *operator[](size_t i) { return items_.data() + i * size_; }
	const T *operator[](size_t i) const { return items_.data() + i * size_; }
	T *data() { return items_.data(); }
	const T *data() const { return items_.data(); }
};

template <typename T, typename Gen>
auto random_matrix(size_t N, Gen &generator) {
	matrix<T> matrix(N);
	for (size_t i = 0; i < N; i++)
		std::generate(matrix[i], matrix[i] + N, [&generator] {
			return 1; //	std::uniform_real_distribution<T>{}(generator);
		});
	return matrix;
}

template <typename T> auto transpose(matrix<T> &m) {
	size_t N = m.size();
	for (size_t i = 0; i < N; i++) {
		for (size_t j = 0; j < i; j++) {
			std::swap(m[i][j], m[j][i]);
		}
	}
}

template <typename T>
std::ostream &operator<<(std::ostream &out, const matrix<T> &m) {
	for (size_t i = 0; i < m.size(); i++) {
		std::copy(m[i], m[i] + m.size(), std::ostream_iterator<T>{out, " "});
		out << '\n';
	}
	return out;
}
std::vector<double> row(2000 * 2000);
std::vector<double> result(2000 * 2000);
matrix<double> A{2000}, B{2000};
matrix<double> slave(mpi::communicator &world, size_t N = {}) {
	mpi::broadcast(world, N, 0);
	size_t row_count = N / world.size();
	size_t chunk = row_count * N;
	mpi::scatter(world, A.data(), row.data(), chunk, 0);
	mpi::broadcast(world, B.data(), N * N, 0);
	for (size_t k = 0; k < row_count; k++) {
		size_t shift = k * N;
		for (size_t i = 0; i < N; i++) {
			for (size_t j = 0; j < N; j++) {
				result[shift + i] += row[shift + j] * B[i][j];
			}
		}
	}
	mpi::gather(world, result.data(), chunk, A.data(), 0);
	return A;
}

void master(mpi::communicator &world, size_t size) {
	std::random_device rand;
	std::mt19937 gen(rand());
	A = random_matrix<double>(size, gen);
	B = random_matrix<double>(size, gen);
	using clock = std::chrono::steady_clock;
	auto start = clock::now();
	transpose(B);
	auto result = slave(world, size);
	auto duration = clock::now() - start;
	std::cout
	    << result << '\n'
	    << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count()
	    << "ms" << std::endl;
}

int main(int argc, char *argv[]) {
	mpi::environment env{argc, argv};
	mpi::communicator world;
	std::cout << "Started worker " << world.rank() << " of " << world.size()
	          << std::endl;
	if (world.rank() == 0) {
		size_t size;
		if (!parse_opts(argc, argv, &size)) {
			return 1;
		}
		master(world, size);
	} else {
		slave(world);
	}
	//	0 200 400 600 800 1000 1200 1400 1600 1800 2000
	//	0 4   37  111 250 472  797  1248 1901 2661 3665
}
