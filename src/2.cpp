#include <algorithm>
#include <array>
#include <chrono>
#include <iostream>
#include <iterator>
#include <memory>
#include <omp.h>
#include <random>
#include <utility>

namespace {
template <typename T> struct alignas(64) cache_aligned {
	T value;
	cache_aligned(T value) : value{std::move(value)} {};
};
template <typename T, typename Clock> struct tester {
	template <size_t... Ns> struct multitest {
		template <size_t... JobSizes> struct with {
			template <size_t N, size_t JobSize> struct test {
				auto operator()(std::ostream &out) const {
					constexpr double l = 1;
					size_t found = 0;
					std::random_device rand;
					using generator = std::mt19937;
					std::vector<cache_aligned<generator>> generators;
					for (size_t i = 0; i < 1; i++)
						generators.emplace_back(generator{rand()});
					auto start = Clock::now();
#pragma omp parallel for num_threads(4) reduction(+ : found) schedule(static, JobSize)
					for (size_t i = 0; i < N; i++) {
						auto thread_id = omp_get_thread_num();
						auto h =
						    std::uniform_real_distribution<T>{0, l}(generators[thread_id].value);
						auto phi = std::uniform_real_distribution<T>{0, M_PI}(
						    generators[thread_id].value);
						if (h <= l * sin(phi))
							found++;
					}
					auto duration = Clock::now() - start;
					out << 2.0l * N / found << '\n'
					    << std::chrono::duration_cast<std::chrono::milliseconds>(duration)
					           .count()
					    << std::endl;
					return duration;
				}
			};
			auto operator()(std::ostream &out) const {
				using result_type =
				    std::array<typename Clock::duration, sizeof...(JobSizes)>;
				result_type result{{test<Ns, JobSizes>{}(out)...}};
				for (int i = 0; i < 3; i++) {
					result_type temp{test<Ns, JobSizes>{}(out)...};
					std::transform(result.begin(), result.end(), temp.begin(), result.begin(),
					               [](auto &a, auto &b) { return std::min(a, b); });
				}
				return result;
			}
		};
		multitest(std::index_sequence<Ns...>) {}
		template <size_t... JobSizes>
		auto operator()(std::ostream &out, std::index_sequence<JobSizes...>) {
			return with<JobSizes...>{}(out);
		}
	};
}; // namespace
template <size_t Multiplier, size_t... Ns>
auto multiply_sequence(std::index_sequence<Ns...>) {
	return std::index_sequence<(Ns * Multiplier)...>{};
}
} // namespace

int main() {
	std::ios_base::sync_with_stdio(false);
	// threads:
	// 1 2 4 8 16 32 64 128
	// 41215, 21173, 16487, 16343, 16378, 16408, 16586, 17216
	// 4 threads prefered
	// auto durations =
	//     tester<double, std::chrono::steady_clock>::multitest{
	//         std::index_sequence<100000000, 100000000, 100000000, 100000000,
	//                             100000000, 100000000, 100000000, 100000000,
	//                             100000000, 100000000, 100000000, 100000000>()}(
	//         std::cout, std::index_sequence<1 << 7, 1 << 9, 1 << 11, 1 << 13,
	//                                        1 << 15, 1 << 17, 1 << 19, 1 << 21,
	//                                        1 << 23, 1 << 25, 1 << 27, 1 <<
	//                                        29>{});
	// static
	// 2279 2247 2235 2252 2239 2227 2271 2240 2257 2212 4304 4294
	// dynamic
	// 2247 2247 2249 2285 2230 2262 2239 2263 2286 2238 4298 4282
	// guided
	// 2243 2254 2247 2236 2235 2264 2248 2257 2253 2238 4360 4294

	auto durations =
	    tester<double, std::chrono::steady_clock>::multitest{
	        std::index_sequence<100000000, 200000000, 300000000, 400000000,
	                            500000000, 600000000, 700000000, 800000000,
	                            900000000, 1000000000>()}(
	        std::cout,
	        std::index_sequence<1 << 25, 1 << 25, 1 << 25, 1 << 25, 1 << 25,
	                            1 << 25, 1 << 25, 1 << 25, 1 << 25, 1 << 25>{});
	// result
	// 1952 3666 5705 6734 8636 10294 12268 13428 15150 16969
	// 4014 8073 12086 16090 20095 24110 28249 32267 36280 40252

	// 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000
	// 0 0 0 0 0 4 48 418 1940 16948
	// 0 0 0 0 0 4 44 404 4066 40534
	for (auto &dur : durations) {
		std::cout
		    << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count()
		    << ' ';
	}
}
