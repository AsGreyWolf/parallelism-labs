# .PHONY: all
CXX = mpic++
CXXFLAGS = -Wall -Wextra -Weffc++ -std=c++17 -pedantic -O3 -march=native -flto -fopenmp
LDFLAGS = -flto -march=native -O3 -lboost_program_options -lboost_mpi -lboost_serialization -fopenmp

SRCS = $(wildcard src/*.cpp)
OBJS = $(SRCS:.cpp=.o)
BINS = $(notdir $(SRCS:.cpp=.lab))

all: $(BINS)

%.lab: src/%.o
	$(CXX) $(LDFLAGS) $< -o $@

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@
